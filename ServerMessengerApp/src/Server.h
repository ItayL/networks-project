#ifndef SERVER_H_
#define SERVER_H_

#include "UsersManager.h"


using namespace std;
namespace npl{

class Server : public MThread {

private:
	TCPSocket * socket;
	UsersManager * loginAndRegister;

public:
	Server();
	void listAllRegisterUsers();
	void run();
	void shutdown();
	void printUsersInRoom(string roomName);
	void printOnLineUsers();
	void printActiveSessions();
	void printRooms();
	virtual ~Server();
};

}
#endif /* SERVER_H_ */
