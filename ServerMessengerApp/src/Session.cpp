#include "Session.h"
#include "UsersManager.h"

namespace npl {

Session::Session(Handler* handler, PeerName* peerUserA, PeerName* peerUserB) {
	this->handler = handler;
	this->peerUserA = peerUserA;
	this->peerUserB = peerUserB;
	isActive = true;
	update();
	start();

}
void Session::run() {
	while (isActive) {
		MTCPListener listener;
		listener.add(peerUserA->getPeer());
		listener.add(peerUserB->getPeer());
		TCPSocket * peer = listener.listen(30);
		if (peer != NULL) {
			Command command = NONE;
			map<string, string> data;
			MessengerProtocol::readMSG(peer, command, data);
			PeerName* sender = FindPeer(peer);
			switch (command) {
			case LIST_CONNECTED_USERS:
				handler->onConnectedUserList(peer, data);
				break;
			case EXIT_SESSION:
				close();
				break;
			case LIST_USERS:
				MessengerProtocol::SendMSG(peer, LIST_USERS,UsersManager::getUserPassMap());
				break;

			case DISCONNECT:
			case 0:
				if (this->peerUserA==sender)
				{
					this->peerUserA=NULL;
				}
				else
					this->peerUserB=NULL;
				this->handler->onSessionClose(this,peerUserA,peerUserB);
				delete sender;
				this->close();
				break;

			default:
				MessengerProtocol::SendMSG(peer, COMMAND_NOT_FOUND);

			}
		}
	}
	delete this;
}

void Session::close() {
	MessengerProtocol::SendMSG(peerUserA->getPeer(), EXIT_SESSION);
	MessengerProtocol::SendMSG(peerUserB->getPeer(), EXIT_SESSION);
	this->isActive = false;
	handler->onSessionClose(this, peerUserA, peerUserB);
}

Session::~Session() {

}

PeerName* Session::FindPeer(TCPSocket* peer) {
	if (peer == peerUserA->getPeer())
		return peerUserA;
	return peerUserB;
}

void Session::update() {
	map<string, string> maptoA;
	map<string, string> maptoB;
	string key1,key2,key3;
	key1=IP_KEY;
	key1.append("0");
	key2=PORT_KEY;
	key2.append("0");
	key3=USER_NAME_KEY;
	key3.append("0");
	maptoA[key1]=peerUserB->getPeer()->getDestAddr();
	maptoA[key2]=Global::convertTostring(peerUserB->getPeer()->getDestPort());
	maptoB[key1]=peerUserA->getPeer()->getDestAddr();
	maptoB[key2]=Global::convertTostring(peerUserA->getPeer()->getDestPort());
	maptoA[key3]= peerUserB->getUserName();
	maptoB[key3]=peerUserA->getUserName();
	MessengerProtocol::SendMSG(peerUserB->getPeer(), SESSION_ESTABLISHED, maptoB);
	MessengerProtocol::SendMSG(peerUserA->getPeer(), SESSION_ESTABLISHED, maptoA);
	Global::printMap(maptoA);
	Global::printMap(maptoB);
}

}  // namespace npl

