#include "PeerName.h"

namespace npl{
PeerName::PeerName(string userName,TCPSocket* peerSock) {
	this->userName = userName;
	this->peerSock = peerSock;
}

TCPSocket* PeerName::getPeer(){
	return this->peerSock;
}

string PeerName::getUserName(){
	return this->userName;
}

PeerName::~PeerName() {
}

}
